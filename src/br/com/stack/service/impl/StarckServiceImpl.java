package br.com.stack.service.impl;

import br.com.stack.exception.EmptyException;
import br.com.stack.exception.StackFullException;
import br.com.stack.service.StackService;

/**
 * Implementação do algoritmo de pilha com arranjo de tamanho fixo.
 * Uma exceção é lançada ao tentar realizar um push quando o tamanho 
 * máximo é alcançado.
 * Esta classe implementa o contrato {@link StackService}
 * @author Bruno Ribeiro
 */
public class StarckServiceImpl implements StackService{

	/**
	 * Tamanho padrão da pilha
	 */
	public static final Integer MAX_VALUE = 10;
	
	/**
	 * Tamanho da pilha
	 */
	private Integer maxValue;
	
	/**
	 * Vetor usado para armazenar a pilha
	 */
	private Object stack[];
	
	/**
	 * Indice do elemento no topo da pilha
	 */
	private Integer top = -1;
	
	/**
	 * Inicializa a pilha com o valor padrão
	 */
	public StarckServiceImpl(){
		this(MAX_VALUE);
	}
	
	/**
	 * Inicializa a pilha com um valor determinado
	 * @param max
	 */
	public StarckServiceImpl(Integer max) {
		this.maxValue = max;
		this.stack = new Object[max];
	}

	@Override
	public Integer size() {
		return (maxValue + 1);
	}

	@Override
	public Boolean isEmpty() {
		return maxValue < 0;
	}

	@Override
	public Object top() {
		if (isEmpty())
			throw new EmptyException("Stack is empty");
		return stack[top];
	}

	@Override
	public void push(Object item) {
		if (size() == maxValue)
			throw new StackFullException("Stack overflow");
		
		stack[++top] = item;
	}

	@Override
	public Object pop() throws EmptyException {
	    if (isEmpty())
	    	throw new EmptyException("Stack is empty");
	    
	    Object obj = stack[top];
	    stack[top] = null; 
		return obj;
	}

}
