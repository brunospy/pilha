package br.com.stack.service;

import br.com.stack.exception.EmptyException;

/**
 * Contrato que define os métodos necessários 
 * para implementação do algoritmo Pilha
 * @author Bruno Ribeiro
 * @see EmptyException
 */
public interface StackService {

	/**
	 * @return número de itens da pilha
	 */
	Integer size();
	
	/**
	 * @return True se vazia e false se contiver itens
	 */
	Boolean isEmpty();
	
	/**
	 * @return item do topo da pilha
	 * @exception EmptyException se a pilha estiver vazia
	 */
	Object top();
	
	/**
	 * Insere um novo item na pilha
	 * @param item a ser inserido
	 */
	void push(Object item);
	
	/**
	 * Remove o elemento do topo da pilha
	 * @return Objeto removido
	 * @exception EmptyException se vazia
	 */
	Object pop() throws EmptyException;
	
}
