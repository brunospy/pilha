package br.com.stack.exception;

/**
 * Exceção lançada quando se tenta fazer uma operação de top ou pop
 * sobre uma pilha vazia
 * @author Bruno Ribeiro
 */
public class EmptyException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	
	public EmptyException(String error){
		super(error);
	}
}
