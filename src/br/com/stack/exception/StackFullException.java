package br.com.stack.exception;

/**
 * Exceção lançada quando é feito um push em uma pilha que ja atingiu o 
 * seu limite;
 * @author Bruno Ribeiro
 */
public class StackFullException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public StackFullException(String error){
		super(error);
	}

}
